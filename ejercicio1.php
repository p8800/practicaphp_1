<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1 de la hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
        En este ejercicio vamos a escribir un texto de la página web a través de PHP
        <hr>
        <?php
        
            echo "Este texto esta escrito desde el script de PHP.";
            
            echo "Este tesxto también se escribe desde el script de PHP.";
        
        ?>
        <hr>
        Esto esta escrito en HTML normal
        <hr>
        <?php
        
            print("Esta es otra forma de escribir cosas en la web");
        
        ?>
        
    </body>
</html>

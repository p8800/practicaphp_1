<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 de la Hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
       <table width="100%" border="1">
           <tr>
               <td>
                   <?php
                        echo 'Este texto quiero que lo escribas utilizando la función echo de PHP.';
                   ?>
               </td>
               <td>
                   Aquí debe colocar un texto directamente en HTML.
               </td>
           </tr>
           <tr>
               <td>
                   <?php
                        print('Este texto quiero que lo escribas utilizando la función print de PHP.');
                   ?>
               </td>
               <td>
                   <?php
                        echo 'Academia Alpe';
                   ?>
               </td>
           </tr>
       </table>
    </body>
</html>
